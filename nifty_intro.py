import nifty8 as ift
from matplotlib import pyplot as plt
import numpy as np


plt.ion()
position_space = ift.RGSpace([128,128])
harmonic_space = position_space.get_default_codomain()

HT = ift.HarmonicTransformOperator(harmonic_space, position_space)

def sqrtpspec(k):
    return 1./(20. + k**2)

p_space = ift.PowerSpace(harmonic_space)
pd = ift.PowerDistributor(harmonic_space, p_space)
a = ift.PS_field(p_space, sqrtpspec)

A_field = pd(a)
A = ift.makeOp(A_field)
xi = ift.FieldAdapter(harmonic_space,'xi')

args = {
    'offset_mean': 0,
    'offset_std': (1e-3, 1e-6),

    # Amplitude of field fluctuations
    'fluctuations': (1., 0.8),  # 1.0, 1e-2

    # Exponent of power law power spectrum component
    'loglogavgslope': (-3., 1),  # -6.0, 1

    # Amplitude of integrated Wiener process power spectrum component
    'flexibility': (2, 1.),  # 1.0, 0.5

    # How ragged the integrated Wiener process component is
    'asperity': (0.5, 0.4)  # 0.1, 0.5
}

correlated_field = ift.SimpleCorrelatedField(position_space, **args)

# GP = HT @ A @ xi

xi_true = ift.from_random(correlated_field.domain)

s = correlated_field(xi_true)

sky = ift.exp(correlated_field)

sky_true = sky(xi_true)

data_raw = np.random.poisson(sky_true.val)

data = ift.makeField(sky.target, data_raw)

log_likelihood = ift.PoissonianEnergy(data) @ sky

ic_sampling = ift.AbsDeltaEnergyController(name="Sampling (linear)",
                                               deltaE=0.05, iteration_limit=100)

ic_sampling_nl = ift.AbsDeltaEnergyController(name="Sampling (non-linear)",
                                               deltaE=0.05, iteration_limit=10)


ic_newton = ift.AbsDeltaEnergyController(name='Newton', deltaE=0.5,
                                             convergence_level=2, iteration_limit=5)

minimizer = ift.NewtonCG(ic_newton)
minimizer_sampling = (lambda iiter: None if iiter < 3 else ift.NewtonCG(ic_sampling_nl))

n_iterations = 5
n_samples = 5

samples = ift.optimize_kl(log_likelihood, n_iterations, n_samples, minimizer,
                          ic_sampling, minimizer_sampling)
