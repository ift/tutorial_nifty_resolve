# Nifty tutorial for radio interferometric imaging 

This repository provides a hands on tutorial to perform radio imaging using information field theory.

The demo scripts `nifty_interfaces.py` and `nifty_intro.py` give and introduction
into the world of nifty and how to do inference with it.

In addition, three Jupyter notebooks are available:

- `demo_CorrelatedFields.ipynb`: An introduction to the correlated field model, its hyperparameters, and their effect in the statistical properties of the gaussian process.

- `demo_radio.ipynb`: A mock inference task given a simplified VLBI imaging setup using the uv-coverage of the 2017 imaging campaign of the Event Horizon Telescope ([eht](https://eventhorizontelescope.org/)) and an artificially generated sky brightness distribution. 

- `demo_joint_cal_imag.ipynb`: A resolve demo script for joint calibration and imaging. The demo uses VLA data of SN1006.

## Requirements

- [python3](https://www.python.org/) (3.10 or later)
- [matplotlib](https://matplotlib.org/)

- [nifty8](https://gitlab.mpcdf.mpg.de/ift/nifty/-/tree/NIFTy_8)
- [resolve](https://gitlab.mpcdf.mpg.de/ift/resolve) 

## Installation via pip

To install the packages, simply run for example

```
pip install matplotlib
pip install nifty8
git clone --recursive https://gitlab.mpcdf.mpg.de/ift/resolve.git
pip install resolve
```

or build them from their sources.

## Further information

To get started with the packages, take a look at the projects homepages:

- [https://ift.pages.mpcdf.de/nifty/](https://ift.pages.mpcdf.de/nifty/)
- [ift.pages.mpcdf.de/resolve](https://ift.pages.mpcdf.de/resolve)

Additionally, the package `nifty` provides a variety of explanatory examples for imaging and general Bayesian inference tasks. See [demos](https://gitlab.mpcdf.mpg.de/ift/nifty/-/tree/NIFTy_8/demos) for more information.
