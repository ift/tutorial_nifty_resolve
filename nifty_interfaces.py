import nifty8 as ift
import numpy as np

# Domains / spaces
sp = ift.RGSpace(shape=(2, 3),
                 distances=(1., 1.5))
print(sp.shape)
print(sp.distances)
print(sp.total_volume)
print(sp.scalar_dvol)
print()

sp1 = ift.UnstructuredDomain(4)
print(sp1)
print()

# Fields
d_arr = np.array([12.1, 4.3, 21., 1110.])
d = ift.makeField(sp1, d_arr)
print(d)
s = ift.from_random(sp)  # draws std normal samples
print(s)
print(s.integrate())
print(s.sum())
print()
print(s.domain)
print(s.val)
#print(s.val_rw())


# Product domains
dom = ift.makeDomain((sp, sp1))
print(dom)
print(dom.shape)
print(dom.size)

# MultiDomain: dictionaries of Domains
dom = ift.makeDomain({"key0": sp, "key1": sp1})
print(dom)
print(dom.size)

mfld = ift.from_random(dom)
mfld = mfld.sin()
print(mfld.domain)
print(mfld.val)



print()
print()
print()
print()
# Operators
sp = ift.RGSpace((2, 2), (1, 1))
mask = ift.makeField(sp,
         np.array([[False, False],
                   [False, True]])
         )
op = ift.MaskOperator(mask)
print(op)
print(op.domain)
print(op.target)

inp = ift.from_random(op.domain)
print("Input")
print(inp)
print("Output")
print(op(inp))
